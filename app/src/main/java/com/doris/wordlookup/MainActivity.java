package com.doris.wordlookup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";

    private TextView mInfoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editText = findViewById(R.id.ed_word_search);
        Button searchButton = findViewById(R.id.btn_search);
        Button exitButton = findViewById(R.id.btn_exit);

        mInfoTextView = findViewById(R.id.tv_error);


        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final WordAdapter adapter = new WordAdapter(this);
        recyclerView.setAdapter(adapter);


        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String englishWord = editText.getText().toString();
                mInfoTextView.setVisibility(View.GONE);

                if (TextUtils.isEmpty(englishWord)) {
                    Toast.makeText(MainActivity.this, "Enter a word please", Toast.LENGTH_LONG).show();
                    return;
                }

                List<String> words = searchForTransaltion(englishWord);

                adapter.setWords(words);

                if(words.size() == 0){
                    mInfoTextView.setText("Word Not found");
                    mInfoTextView.setVisibility(View.VISIBLE);
                }


            }
        });
    }

    private List<String> searchForTransaltion(String word) {

        String line = "";
        List<String> wordList = new ArrayList<>();
        try {
            InputStream inputStream = getAssets().open("swahili_db.csv");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);


            while ((line = bufferedReader.readLine()) != null) {
//                Log.d(TAG, "searchForTransaltion: "+ line);
                List<String> words = Arrays.asList(line.split(","));
//                Log.d(TAG, "searchForTransaltion: " + words);

//
                if (words.size() == 2) {
                    if (word.toLowerCase().trim().equals(words.get(1).toLowerCase())) {
                        wordList.add(words.get(0));
                    }

                }

                if (wordList.size() == 10) {
                    break;
                }

            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return wordList;
    }

}
